$(function() {
    var element_id = null;

    $("#brochure-btn").on("click", function() {
        if (element_id != null && element_id != "brochure") {
            $("#"+element_id).hide();
        } 
        element_id = "brochure";
        $("#"+element_id).slideDown();
    });

    $(".boll").on("click", function() {
        var id = event.target.id;

        if (element_id != null && element_id != id) {
            $("#"+element_id).hide();
        }

        element_id = "boll-"+id;
        $("#"+element_id).slideDown();
    })
});